// Was having occasional issues with getting a value for out textbox, so I made sure to implement this on load, to guaranteed a value.
function init() {
    document.getElementById("textFrame").value = "0";    

}    

// initiation of our variables to be used throughout our calculations.
var counting = 0;
var mathOpps = 0;
var iteration = 0;
var timeDisplay = "";
var timeSeconds = 0;
var timeMinutes = 0;
var timeHours = 0;
var onOff = 0;
var interStop;
var n = 0;

// Our primary function whos on-click initiates a display and calculation with certain restrictions to our microwave display.
function inputDisplay(n) {
    // Initiates our display to find out what our value is for our text box.
    var display = document.getElementById("textFrame").value;
    
    // Checks to see if our display is empty, or in this case, set to a default of 0, and does not exceed 99 hours, 59 minutes, and 59 seconds.
    if ((display == "0")&&(counting < 100000)) {
        // A switch case built to set our inital display of seconds on the calulator, it makes it so that we can begin the process of implementing a display.
        switch (n) {
            // For the press of our #0 button. Directly sets the display to 0.   
            case 0:
                display = "0";
                counting = 0;
                break;
                
            // For the press of our #1 button. Directly sets the display to 1.   
            case 1:
                display = "1";
                counting = 1;    
                break;
                
            // For the press of our #2 button. Directly sets the display to 2.   
            case 2:
                display = "2";
                counting = 2;
                break;
                
            // For the press of our #3 button. Directly sets the display to 3.   
            case 3:
                display = "3";
                counting = 3;
                break;
                
            // For the press of our #4 button. Directly sets the display to 4.  
            case 4:
                display = "4";
                counting = 4;
                break;
                
            // For the press of our #5 button. Directly sets the display to 5.   
            case 5:
                display = "5";
                counting = 5;
                break;
                
            // For the press of our #6 button. Directly sets the display to 6.     
            case 6:
                display = "6";
                counting = 6;
                break;
                
            // For the press of our #7 button. Directly sets the display to 7.     
            case 7:
                display = "7";
                counting = 7;
                break;
                
            // For the press of our #8 button. Directly sets the display to 8.     
            case 8:
                display = "8";
                counting = 8;
                break;
                
            // For the press of our #9 button. Directly sets the display to 9.     
            case 9:
                display = "9";
                counting = 9;
                break;
                
            // For the press of our "reset button. Directly sets the display to our initial 0.     
            case reset:   
                display = "0";
                counting = 0;
                break;

            case 'popcorn':
            counting = 230
            break;

            case 'pizza':
            counting = 200
            break;

            case 'bakedpotato':
            counting = 300
                break;

           case 'start':
                construct();
                break;
                // call construct on button click
                
            case 'stop':
                timeStop();
                break; 
                // call time stop on button click
            
        }
    }
    // Checks to see if our display is not empty, or in this case, set to a default of 0, and does not exceed 99 hours, 59 minutes, and 59 seconds.
    else if ((display != "0")&&(counting < 100000)) {
        // Sets our iteration up for multiples of 10, simply to 'push' the number into the next space (e.g. 100 = 1 minute, and 00 seconds. and 1000 = 10 minutes, and 00 seconds.).
        iteration = 10;
        // A switch case built to set our display into seconds and minutes, it uses our 'counting' variable to properly establish the relation betweens number size, and converting to Hours, Minutes, and Seconds later. (Note the addition to display is only to pass the check in our if and else if, it does not actually get displayed.)
        switch (n) {
            // For the press of our #0 button. Directly sets the display to 10 times our prior number and adds 0 to it.     
            case 0:
                display += "0";
                counting = (counting * iteration);
                break;
                
            // For the press of our #1 button. Directly sets the display to 10 times our prior number and adds 1 to it.     
            case 1:
                display += "1";
                counting = (counting * iteration);        
                counting += 1; 
                break;
                
            // For the press of our #2 button. Directly sets the display to 10 times our prior number and adds 2 to it.     
            case 2:
                display += "2";
                counting = (counting * iteration);
                counting += 2;
                break;
                
            // For the press of our #3 button. Directly sets the display to 10 times our prior number and adds 3 to it.  
            case 3:
                display += "3";
                counting = (counting * iteration); 
                counting += 3;
                break;
                
            // For the press of our #4 button. Directly sets the display to 10 times our prior number and adds 4 to it.      
            case 4:
                display += "4";
                counting = (counting * iteration);
                counting += 4;
                break;
            case 5:
                
            // For the press of our #5 button. Directly sets the display to 10 times our prior number and adds 5 to it.      
                display += "5";
                counting = (counting * iteration);
                counting += 5;
                break;
                
            // For the press of our #6 button. Directly sets the display to 10 times our prior number and adds 6 to it.      
            case 6:    
                display += "6";
                counting = (counting * iteration);
                counting += 6;
                break;
                
            // For the press of our #7 button. Directly sets the display to 10 times our prior number and adds 7 to it.      
            case 7:
                display += "7";
                counting = (counting * iteration);
                counting += 7;
                break;
            // For the press of our #2 button. Directly sets the display to 10 times our prior number and adds 2 to it.      
            case 8:
                display += "8";
                counting = (counting * iteration);
                counting += 8;
                break;
            // For the press of our #2 button. Directly sets the display to 10 times our prior number and adds 2 to it.      
            case 9:
                display += "9";
                counting = (counting * iteration);
                counting += 9;
                break;
            
            // For the press of our "reset" button. Directly sets the display to 0 so it will go back into our inital for statement. Hard resets our variables to allow a fresh recount.
            case reset:   
                display = "0";
                counting = 0;
                iteration = 0;
                break;

            case 'popcorn':
           counting = 230;
           break;

            case 'pizza':
            counting = 200;
            break;

            case 'bakedpotato':
            counting = 300;
            break;
                
            case 'start':
            construct();
            break;
            // call construct on button clic    
            case 'stop':
            timeStop();
            break;
            // call time stop on button click
        }
    }
    
    // Checks to see if our counting variable exceeds 9,999,999 or in this case (99 hours: 99 minutes: 99 seconds.)
    else if (counting >= 100000) {
        // does a quick check of our display to make sure that it is not default.
        if (display != 0) {
            // This switch case is simply designed to check if you are trying to add a number or reset the microwave.
            switch (n) {
                // If the choice is to reset the calculator will let you reset it without causing and errors or encountering any issues.
                case 'r':
                    display = "0";
                    counting = 0;
                    iteration = 0;
                    break;
                // If the choice is anything else, the calculator will not let you exceed a time of 99 hours: 99 minutes : and 99 seconds and autosets your focus to the reset button.  
                default:
                    alert("You cannot exceed a time of 99 hours, 99minutes, and 99 seconds.");
                    document.getElementById("reset").focus();
                    break;
            }
        }
    }

// Math Operation to convert input time to seconds. 
  //if(onOff=1)
   { // Assigns a new variable to hold onto our counting method while we set up out display.
    mathOpps = counting;
    // Goes through Hours, Minutes, and Seconds and finds the floor, to perserve us a remainder for counting.
    timeHours = Math.floor(counting/10000);
    counting = counting - (timeHours * 10000);
    timeMinutes = Math.floor(counting/100);
    counting = counting - (timeMinutes * 100);
    timeSeconds = counting;
    // Re-assigns counting to the original number, so we can increment it with additional button presses.
    counting = mathOpps;
    
// Checks to see if we have Hours or Minutes to be displayed.   
    if ((timeHours == 0)&&(timeMinutes == 0)) {
        display = (timeSeconds);
        document.getElementById("textFrame").value = display;
    }   
// Checks to see if we have Hours to be displayed, if we have minutes.
    else if (timeHours == 0) {
        display = (timeMinutes + " : " + timeSeconds);
        document.getElementById("textFrame").value = display;
    }
// Posts all hours, minutes, and seconds to be displayed.     
    else {
        display = (timeHours + " : " + timeMinutes + " : " + timeSeconds);
        document.getElementById("textFrame").value = display;
    }
  }  

}

function construct(){
            if (!onOff)
            {
                onOff=1;
                // switch on onOff
                interStop = setInterval(timer, 1000);  
            }
            else if (onOff)
            {
                //alert(display);
                counting = display;
                // pull counting value from display
                interStop = setInterval(timer, 1000);
                
            }
        }

    function timeStop()
            {
                clearInterval(interStop);
                // clear interval in interStop
                onOff=0;
                // switch off onOff
                counting = timeSeconds + 100*timeMinutes + 10000*timeHours;
                // format count to hr min seconds and set place
            }
      
    function timer(){
                // function count down min sec hrs
               if (timeSeconds != 0){
                    document.getElementById("textFrame").value = timeHours + " : " + timeMinutes + " : " + timeSeconds;
                    display = (timeHours + " : " + timeMinutes + " : " + timeSeconds);
                    timeSeconds--;
                   // set seconds display and iterate
                }
                else if((timeSeconds == 0) && (timeMinutes > 0)){
                    document.getElementById("textFrame").value = timeHours + " : " + timeMinutes + " : " + timeSeconds;
                    display = (timeHours + " : " + timeMinutes + " : " + timeSeconds);
                    timeMinutes--;
                    timeSeconds = 60;
                    // set minutes display and iterate
                }
                else if((timeMinutes == 0) && (timeHours > 0)){
                    document.getElementById("textFrame").value = timeHours + " : " + timeMinutes + " : " + timeSeconds;
                    display = (timeHours + " : " + timeMinutes + " : " + timeSeconds);
                    timeHours--;
                    timeMinutes = 60;
                    // set hours display and iterate
                } 
                else{
                    document.getElementById("textFrame").value = "Done";
                    setInterval("blink()",250);
                    // display "Done"
             }
       

    }

 function blink() {
        var elm = document.getElementById("textFrame");
        if ( document.getElementById("textFrame").value == "Done"){
            n++;
                if (n % 2 == 0) {
                    elm.style.color = "#ff0000";
                }
                else if (n % 2 == 1) {
                    elm.style.color = "#00ff00";
                }

            
        }
     else {
         elm.style.color = "black";  
     }

 }


