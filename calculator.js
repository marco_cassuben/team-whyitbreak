//When buttons are pressed, set the value of button pressed to the text box
//
var firstValue = 0;         //first entered value
var secondValue = 0;        //second entered value(after the operator button is pressed)
var operator = "";          //default to ""
var outputValue = 0;        //output value
var decimalCheck = true;    //boolean to check if only one decimal is added
var outputbox = ""; //output box(where the numbers will be shown)
var signValue = true;   //boolean for the sign of the value
var finalValue = "";
var continueOperator = false;

//set the output to the text box
function setTextBox()
{
    outputbox = document.getElementById("textbox");
}

//displays the numbers that the user presses buttons for
function displayInput(number)
{ 
        //if error is still on screen, clear it and then add number to texbox
        if(outputValue == "Error")
            {
                clearInputs();
                outputbox.value += number;
            }
        //prevents multiple 0's from being added if the number is already 0
        else if((outputbox.value == "0") && (number == 0))
            {
                //do nothing   
            }
        //user inputs number before operator and we must clear the values
        else if((finalValue != "") && (continueOperator == false))
          {
              continueOperator = false;
              clearInputs();
              outputbox.value += number;
          }
         else if(outputbox.value.length > 10)
             {
                 
             }
        else{
    
            outputbox.value += number;}
}

//function to only let the user input one decimal per
//value entered
function decimalInput()
{
    if(decimalCheck == true)
        {
            outputbox.value += ".";
            decimalCheck = false;
        }
}



//function to set the operator
function operatorValue(op)
{
   var currentValue = outputbox.value; //get current value of the window
    if (finalValue != "")
        {
            firstValue = finalValue;  //set the first value to the current value
            continueOperator = true;

        }
    else if(currentValue != "")
        {
                firstValue = currentValue;  //set the first value to the current value
          
        }  

    operator = op;    //set operator to the one specified in the parameter
    outputbox.value = "";
    
}


//displays the final value
function displayValue()
{
    //sets the second value based on the current input in the text box
    secondValue = document.getElementById("textbox").value;
    //takes the operator given from the functions and does the operation
    switch(operator){
        case "+":
                outputValue = +firstValue + +secondValue;
                break;
        case "-":
                outputValue = +firstValue - +secondValue;
                break;
        case "*":
                outputValue = +firstValue * +secondValue;
                break;
        case "/":
                if(secondValue == 0)
                    {
                        errorValue();
                    }
                else{
                    outputValue = +firstValue / +secondValue;
                    }
                break;  
        default:
                break;
            }       
    //outputs the value to the textbox
  outputbox.value = outputValue;
  finalValue = outputValue;
  continueOperator = false;
}

//function to clear inputs and reset everything to the default values
function clearInputs()
{
    firstValue = 0;         //first entered value
    secondValue = 0;        //second entered value(after the operator button is pressed)
    operator = "";          //default to ""
    outputValue = 0;        //output value
    decimalCheck = true;    //boolean to check if only one decimal is added
    signValue = true;       //boolean for the sign of the value
    outputbox.value = "";
    finalValue = "";
    continueOperator = false;

}

//divide by 0 error check, sets values to default and changes
//textbox to display error
function errorValue()
{
    outputValue = "Error";          
}

//changes the sign of the current input
function changeSign()
{
    //adds the "-" to current value if the number is not 0
    if(outputbox.value != 0)
        {
            //if the number on the screen is not negative then add '-'
            if(outputbox.value.indexOf('-') === -1)
                {
                 outputbox.value = "-" + outputbox.value;
                 signValue = false;
                }
            else{
                //otherwise remove the negative
                outputbox.value = outputbox.value.replace('-',"");
                signValue = true; 
            }
                
        }

}

//limit the numbers in text

function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}